// "The problem is this. We want the React Docker container to use the files that we have on our file system. That's why we have use the volumes in the docker-compose.yml file. It makes the Docker container think that it has an /app directory, when really it sees our files in the ghi/attendees directory." They point at the downward dotted line from /app to ./ghi/attendees. "The React development server watches for when you change files in what it thinks is the /app directory. They point to the dashed line from the message at the bottom to the big X on the inner rectangle, "But when Windows sees that you change the file, there's a bug that prevents it from getting back to the React Docker container."

// "How do we fix it?"

// "We're going to use another feature in the React development server to monitor for file changes in a different way. We have to add a set-up script so that the React development server will use the other way. Then, we'll change the docker-compose.yml to enable it and run it. Hold on a moment, please." He gets up and walks over to his computer. You watch him lean over his computer. A couple of moments later, you get three Slack messages.

// When he gets back to your desk, he says, "The first Slack message is the script to run. It's written in JavaScript for Node. Create a file named 'setup.js' in the ghi/attendees directory and paste it in there."

// You follow his instructions, creating the ghi/attendees/setup.js file and copying this JavaScript into it:

const { readFileSync, writeFileSync } = require('fs');
const path = require('path');

console.log('Setup reports node environment as', process.env.NODE_ENV);

if (process.env.NODE_ENV === 'development') {
  const configPath = path.resolve('./node_modules/react-scripts/config/webpack.config.js');
  const config = readFileSync(configPath, 'utf8');

  console.log('config text exists', config.length);

  if (!config.includes('watchOptions')) {
    if (config.includes('performance: false,')) {
      const newConfig = config.replace(
        'performance: false,',
        `performance: false,

/* INJECTED BY CUSTOM SETUP SCRIPT */
watchOptions: { aggregateTimeout: 200, poll: 1000, ignored: '**/node_modules', },
        `
      );
      writeFileSync(configPath, newConfig, 'utf8');
      console.log('content written to', configPath);
    } else {
      throw new Error(`Failed to inject watchOptions`);
    }
  } else {
    console.log('already contains watch options.');
  }
}

//"How'd you know how to write this script?" you ask.

// "Well, I couldn't get the React development server to pick up on changes. I spent about 30 minutes reading issues on GitHub and posts on StackOverflow. I cobbled it together from what I read on there. What it does is look at a file called 'webpack.config.js', which is what Create React App uses to build the application. Then, it adds some options to the file if they don't exist, but only if you're running it in development mode."

// While he's explaining, you scan the code. You're not familiar with Node.js, but the JavaScript reads like you may be able to figure it out given an hour of time and some good documentation.

// "Now," Malik continues, "We need to enable the script. In the docker-compose.yml file, add the NODE_ENV environment variable that I sent to you."

// You look at the second Slack message and change the docker-compose.yml file to have these three environment variables for the react-attendees service:

// environment:
//   - HOST=0.0.0.0
//   - PORT=3001
//   - NODE_ENV=development
// "The last thing we have to do is run that script before we start the React development server. That's the third Slack message I sent you. You have to update the package.json file in the ghi/attendees directory. It's in the 'scripts' dictionary."

// You look at the last Slack message which reads:

// "start": "node ./setup.js && react-scripts start",
// You open the ghi/attendees/package.json file, start scanning the lines of JSON until you see the "scripts" section.

// "scripts": {
//   "start": "react-scripts start",
//   "build": "react-scripts build",
//   "test": "react-scripts test",
//   "eject": "react-scripts eject"
// },
// You change the line for the "start" key to look like what Malik sent to you in the last Slack message.

// "That will run the set-up script before it runs the React development server. Just stop your services, get rid of the containers, then build them and bring them back up."

// You type Control+C in your terminal to stop the services. You delete all of the containers, build them, and bring them up.

// docker container prune -f
// docker-compose build
// docker-compose up
// The output from all of the different services start scrolling past on your terminal. "This is going to take a couple of moments," Malik says, his eyes intent on the different log messages. "If you don't mind, will you open Docker Desktop so we can see just the messages from the react-attendees service?"

// You open Docker Desktop, go to the Containers/Apps tab, and expand the service group. You search the list of services until you find the one with "react-attendees" in it. You click on that one.

// You see some messages in there, already.

// > attendees@0.1.0 start
// > node ./setup.js && react-scripts start

// Setup reports node environment as development
// config text exists 32177
// content written to /app/node_modules/react-scripts/config/webpack.config.js
// Malik sees it, too. "That's just the beginning. We have to wait for the message that the local server has started." You both sit and watch the messages. Malik was making an understatement, you think. A "couple of moments" indeed!

// After some more log messages, you see the message that it the local development server has started.

// You can now view attendees in the browser.
// "Let's test it out," Malik says. "If you don't mind, close the browser tab and open a new one to localhost:3001." You do so and see the spinning React logo, again. "Ok. Open the App.js file in the ghi/attendees/src directory. Change the line that reads 'Edit <code>src/App.js</code> blah blah blah' to 'My code reloads'." You make the change and the Web page refreshes by itself to show you the new text.

// "Wow! That's neat!" you say.

// "I agree. Now, if you have to stop and restart your services, make sure you refresh the Web page for your React app to allow it to reconnect to the new instance of the running server. And always watch the log statements to see when it's running. For whatever reason, it takes a long while for the service to come up on Windows."

// Malik continues, "Also, you have to sometimes wait a little while for the first time it recompiles your code after you make a change. Sometimes up to a minute. But after that first recompile, it should be fast after that. Just keep watching the logs in Docker Desktop to see what it's doing. if things seem to have stopped working."

// "Sounds do-able. What's next?"