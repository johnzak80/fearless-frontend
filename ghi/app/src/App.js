import Nav from "./Nav";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendConferenceForm from "./AttendConferenceForm";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import PresentationForm from "./PresentaionForm";
import MainPage from "./MainPage";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <Router>
      <div className="container">
        <Nav />
        <Routes>
          <Route
            path="attendees/new"
            element={<AttendConferenceForm />}></Route>
          <Route path="conferences/new" element={<ConferenceForm />}></Route>
          <Route path="location/new" element={<LocationForm />}></Route>
          <Route
            path="attendees"
            element={<AttendeesList attendees={props.attendees} />}></Route>
          <Route path="presentation/new" element={<PresentationForm />}></Route>
          <Route path="" element={<MainPage />}></Route>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
