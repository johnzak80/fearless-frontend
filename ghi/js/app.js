
function createCard(name, description, pictureUrl, start, end, location) {
    return `
    <div class= "col">
        <div class="card shadow mb-3 ">
            <img src="${pictureUrl}" class="card-img-top " style="height:45vh;">
            <div class="card-body">
                <h3 class="card-title">${name}</h3>
                <span class="text-muted"> ${location}</span>
                <p class="card-text">${description}</p>
            </div>
        <div class="card-footer">
            <span class="text-muted">${start} - ${end}</span>
      </div>
        </div>
    </div>

    `;
  }
  function createAlert() {
    return `
    <div class="alert alert-primary" role="alert">
        Error
    </div>`
}




  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

  
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
  
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const location = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const start = new Date(details.conference.starts).toLocaleDateString();
            const end = new Date(details.conference.ends).toLocaleDateString();
            const html = createCard(title, description, pictureUrl, start, end, location);
            const column = document.querySelector('.col');
            column.innerHTML += html;
            console.log(details.conference);
            
  
          }
        }
  
      }
    } catch (e) {
        console.error(createAlert(e));
   
    }
  
  });