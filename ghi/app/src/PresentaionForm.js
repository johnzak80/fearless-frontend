import React, { useEffect, useState } from "react";

const PresentatonForm = () => {
  const [conferences, setConferences] = useState([]);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [title, setTitle] = useState("");
  const [synopsis, setSynopsis] = useState("");
  const [conference, setConference] = useState("");

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };
  const handleEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
  };

  const handleCompanyNameChange = (event) => {
    const value = event.target.value;
    setCompanyName(value);
  };

  const handleTitleChange = (event) => {
    const value = event.target.value;
    setTitle(value);
  };

  const handleSynopsisChange = (event) => {
    const value = event.target.value;
    setSynopsis(value);
  };

  const handleConfereceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  };

  const fetchData = async () => {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.presenter_name = name;
    data.company_name = companyName;
    data.presenter_email = email;
    data.title = title;
    data.synopsis = synopsis;
    data.conference = conference;

    const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();
      console.log(newPresentation);
    }

    setName("");
    setCompanyName("");
    setEmail("");
    setConference("");
    setSynopsis("");
    setTitle("");
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handleNameChange}
                  placeholder="Presenter Name"
                  required
                  type="text"
                  id="name"
                  name="presenter_name"
                  className="form-control"
                  value={name}
                />
                <label htmlFor="name">Presenter Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleEmailChange}
                  placeholder="Presnter Email"
                  required
                  type="email"
                  id="email"
                  name="presenter_email"
                  className="form-control"
                  value={email}
                />
                <label htmlFor="email">Presenter Email</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleCompanyNameChange}
                  placeholder="Comany Name"
                  type="text"
                  id="companyName"
                  name="company_name"
                  className="form-control"
                  value={companyName}
                />
                <label htmlFor="companyName">Company Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleTitleChange}
                  placeholder="Title"
                  required
                  type="text"
                  id="title"
                  name="title"
                  className="form-control"
                  value={title}
                />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label
                  htmlFor="exampleFormControlTextarea1"
                  className="form-label">
                  Synopsis
                </label>
                <textarea
                  onChange={handleSynopsisChange}
                  className="form-control"
                  id="synopsis"
                  name="synopsis"
                  rows="3"
                  value={synopsis}></textarea>
              </div>
              <div className="mb-3">
                <select
                  onChange={handleConfereceChange}
                  required
                  id="conference"
                  name="conference"
                  className="form-select"
                  value={conference}>
                  <option value="">Choose a conference</option>
                  {conferences.map((conference) => {
                    return (
                      <option key={conference.id} value={conference.id}>
                        {conference.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default PresentatonForm;
